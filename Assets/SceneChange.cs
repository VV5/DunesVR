﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneChange : MonoBehaviour
{
    public Animator anim;
    public Image img;
    Color col = Color.black;

    private void Start()
    {
        OVRManager.HMDUnmounted += () => HMDUnmountedd();
        col.a = 1;
        img.GetComponent<Image>().color = col;
        StartCoroutine(Fade());
        StartCoroutine(ChangeScene());
    }

    void HMDUnmountedd()
    {
        SceneManager.LoadScene("SplashScreen");
    }

    IEnumerator Fade()
    {
        anim.SetBool("Fade", true);
        yield return new WaitUntil(() => img.color.a == 1);
    }

    IEnumerator ChangeScene()
    {
        AsyncOperation ao = SceneManager.LoadSceneAsync("Instructions");
        ao.allowSceneActivation = false;
        yield return new WaitForSeconds(12);
        ao.allowSceneActivation = true;
    }    
}
