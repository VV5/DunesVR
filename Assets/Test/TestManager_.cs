﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestManager_ : MonoBehaviour
{
    Animator anim;
    public GameObject prefab;
    float timer;
	Vector3 pos =  Vector3.zero;
    Vector3[] positionArray = new[] { new Vector3(285f, 5f, -280f), new Vector3(-327f, 5f, -280f), new Vector3(-330f,5f,232f), new Vector3(307f,5f,322f)};
    float[] rotation_ = new [] { 90.0f, 180.0f, 270.0f, 0.0f};
    float RandomRotation;
  
    void Start ()
    {
       
		poolManager.instance.CreatePool (prefab, 1);
        timer = 20.0f;
    }


	void Update ()
    {
  
        timer -= Time.deltaTime;
        if (timer <= 0f)
        {
           RandomRotation = rotation_[Random.Range(0, rotation_.Length)];
           pos = positionArray[Random.Range(0, positionArray.Length)];
            poolManager.instance.ReuseObject(prefab, pos, Quaternion.Euler(0,RandomRotation,0));
            prefab.tag = "PoolObject";
            timer = 20.0f;
        }
    }
}
