﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestObject_ : PoolObject
{
    Animator anima;
    float timer;
    Vector3 lastPos = Vector3.zero;
    int AnimationState = Animator.StringToHash("Base Layer.Worm_New");
    float[] _rotation= new[] { 90.0f, 180.0f, 270.0f, 0.0f };
    float RRotation;

    void Start () {
        anima = this.GetComponent<Animator>();
        lastPos = this.transform.position;
        
    }

    void Update()
    {
        
      
        if (lastPos != this.transform.position)
        {
            transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, RRotation, transform.rotation.eulerAngles.z);
            lastPos = this.transform.position;
            //Debug.Log("reset");
            this.gameObject.SetActive(false);
            anima.SetBool("reDo",false);
            this.gameObject.SetActive(true);
            

        }
        AnimatorStateInfo Stateinfo = anima.GetCurrentAnimatorStateInfo(0);
    }
    public override void OnObjectReuse ()
	{
       transform.localScale = Vector3.one;
    }
}
