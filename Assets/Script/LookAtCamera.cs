﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera : MonoBehaviour
{
    public GameObject cam;
    float lockpos = -90f;
    private Vector3 InitialRotation;
    public GameObject audio_;
    private Vector3 transition;




    private void Start()
    {
        InitialRotation = this.transform.rotation.eulerAngles;
        audio_.SetActive(false);
    }
    void Update ()
    {
        transition = transform.rotation.eulerAngles;
        if (cam.transform.position.z > -60f && cam.transform.position.z < 29f && cam.transform.position.x > -19f && cam.transform.position.x < 16f)

        {
            this.transform.LookAt(cam.transform);
            transform.rotation = Quaternion.Euler(lockpos, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
           
            if (transition != transform.rotation.eulerAngles)
            {
                audio_.SetActive(true);
            }
            else
            {
                audio_.SetActive(false);
            }

        }
       
        else
        {
            transform.rotation = Quaternion.Euler(InitialRotation);
            transition = InitialRotation;
            audio_.SetActive(false);
           
        }
        
    }
}
