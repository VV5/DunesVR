﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(CharacterController))]
public class MovementVR : MonoBehaviour {

	public float speed = 3.0f;

	public bool moveForward;

	private CharacterController controller;


	private OVRPlayerController OPR;

	private Transform VRHead;


	// Use this for initialization
	void Start () {
		controller = GetComponent<CharacterController> ();
		VRHead = Camera.main.transform;


	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown ("Fire1")) {
			moveForward =  !moveForward;
		
		}

		if (moveForward) {
			Vector3 forward = VRHead.TransformDirection (Vector3.forward);
			controller.SimpleMove (forward * speed);
		
		}
	}
}
