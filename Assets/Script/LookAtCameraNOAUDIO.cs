﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCameraNOAUDIO : MonoBehaviour

{
    public GameObject cam_;
    float lockpos = -90f;
    private Vector3 InitialRotation;
    private Vector3 transition;


    private void Start()
    {
        InitialRotation = this.transform.rotation.eulerAngles;
    }
    void Update()
    {
        if (cam_.transform.position.z > -60f && cam_.transform.position.z < 29f && cam_.transform.position.x > -19f && cam_.transform.position.x < 16f)

        {
            this.transform.LookAt(cam_.transform);
            transform.rotation = Quaternion.Euler(lockpos, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
        }

        else
        {
            transform.rotation = Quaternion.Euler(InitialRotation);

        }

    }
}
