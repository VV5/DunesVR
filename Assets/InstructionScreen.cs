﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InstructionScreen : MonoBehaviour
{
    SceneChange sc;
    public Animator anim_;
    Color col_ = Color.black;
    public Image img_;
    private AsyncOperation at;
    public GameObject rotor_;

    private void Awake()
    {
        rotor_.SetActive(false);
       
     // at = SceneManager.LoadSceneAsync("Scene02");
     // at.allowSceneActivation = false;
    }

    void Start ()
    { 
       OVRManager.HMDUnmounted += () => HMDUnmounte();
       col_.a = 1;
       img_.GetComponent<Image>().color = col_;
       StartCoroutine(FinalScene());
    }
	
    void HMDUnmounte()
    {
        SceneManager.LoadScene("SplashScreen");
    }
   IEnumerator FinalScene()
    {
        
        at = SceneManager.LoadSceneAsync("Scene02");
        at.allowSceneActivation = false;
        yield return new WaitUntil(() => at.progress <= 0.89);
        
    }
    private void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            rotor_.SetActive(true);
            at.allowSceneActivation = true;
        }
    }
}
